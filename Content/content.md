---
title: Table of contents / Distribution
author: Alexander Schoch
header-includes: |
    \usepackage[margin=1in]{geometry}
    \usepackage[light]{CormorantGaramond}
    \usepackage{lipsum}
    \usepackage{fancyhdr}
    \newcommand{\ar}{$\rightarrow$}

numbersections: true
---

\maketitle

NOTE: This table of contents refers to the article, *not* to the presentation.

- intro: together, 300
- What is a smart contract engine? What does it do? \ar Eliane, approx. 500
- How does it work? \ar Alexander 1000
    - Short introduction to the blockchain principle, 300
    - Definitions/Examples: Konsensus/verification (pow, poStake, delegierter pos), 300
- What possibilities does it hold? \ar Eliane, 700
    - Examples (e.g. Buying stuff online, banks, govenments (e.g. political acclamation), etc. ideas?), 600
- Comparison \ar Melanie, 1500
- What dangers are possible? \ar Alex 500
- Ethical aspects, positvie and negative \ar Together 500

# Where to look for sources

- google scolar
