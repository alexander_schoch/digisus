# digisus

This repository is for managing all files for the presentation and the article for the digiSus GESS course.

## Compiling 

The files `Article/SmartContractEngines.tex` and `Presentation/presentation.tex` were compiled using the `lualatex` compiler, downloaded with the `texlive` package under `Gentoo GNU/Linux`. In order to compile the files ourself, it is necessary to download the files `preambleGerman.tex` and `preambleGermanBeamer.tex`, which can be found [here](https://gitlab.com/alexander_schoch/templates.git) (The absolute paths have to be changed afterwards, of course). Additionally, the documents depend on the fonts `CormorantGaramond-Light` and `CormorantGaramond`.


