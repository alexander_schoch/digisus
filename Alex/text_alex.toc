\babel@toc {english}{}
\contentsline {section}{\numberline {\oldstylenums {1}}Was ist ein {\normalfont <<}Smart Contract{\normalfont >>}?}{1}{section.1}
\contentsline {section}{\numberline {\oldstylenums {2}}Funktionsprinzip von Smart Conracts}{1}{section.2}
\contentsline {subsection}{\numberline {\oldstylenums {\oldstylenums {2}.1}}Recall: Funktionsprinzip von Blockchains}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {\oldstylenums {\oldstylenums {2}.2}}Ausweitung von Blockchains auf Smart Contracts}{2}{subsection.2.2}
\contentsline {section}{\numberline {\oldstylenums {3}}Das Potential von Smart Contracts}{2}{section.3}
\contentsline {subsection}{\numberline {\oldstylenums {\oldstylenums {3}.1}}Beispiele}{2}{subsection.3.1}
\contentsline {section}{\numberline {\oldstylenums {4}}Gefahren von Smart Contracts}{3}{section.4}
\contentsline {subsection}{\numberline {\oldstylenums {\oldstylenums {4}.1}}Kriminalität}{3}{subsection.4.1}
\contentsfinish 
