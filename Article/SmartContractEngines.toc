\babel@toc {german}{}
\contentsline {section}{\numberline {\oldstylenums {1}}Einleitung}{3}{section.1}
\contentsline {section}{\numberline {\oldstylenums {2}}Was ist ein {\normalfont <<}Smart Contract{\normalfont >>}?}{3}{section.2}
\contentsline {section}{\numberline {\oldstylenums {3}}Funktionsprinzip von Smart Contracts}{4}{section.3}
\contentsline {subsection}{\numberline {\oldstylenums {\oldstylenums {3}.1}}Recall: Funktionsprinzip von Blockchains}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {\oldstylenums {\oldstylenums {3}.2}}Ausweitung von Blockchains auf Smart Contracts}{5}{subsection.3.2}
\contentsline {section}{\numberline {\oldstylenums {4}}Das Potential von Smart Contracts}{5}{section.4}
\contentsline {subsection}{\numberline {\oldstylenums {\oldstylenums {4}.1}}Kriterien für einen Blockchain-Einsatz}{5}{subsection.4.1}
\contentsline {subsection}{\numberline {\oldstylenums {\oldstylenums {4}.2}}Nutzung spezifischer Eigenschaften}{6}{subsection.4.2}
\contentsline {subsection}{\numberline {\oldstylenums {\oldstylenums {4}.3}}Anwendungsfelder der Blockchain-Technologie}{6}{subsection.4.3}
\contentsline {subsubsection}{\numberline {\oldstylenums {\oldstylenums {\oldstylenums {4}.3}.1}}Finanzbranche}{6}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {\oldstylenums {\oldstylenums {\oldstylenums {4}.3}.2}}Medienindustrie}{7}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {\oldstylenums {\oldstylenums {\oldstylenums {4}.3}.3}}\"Offentlicher Sektor}{7}{subsubsection.4.3.3}
\contentsline {section}{\numberline {\oldstylenums {5}}Gefahren von Smart Contracts}{7}{section.5}
\contentsline {subsection}{\numberline {\oldstylenums {\oldstylenums {5}.1}}Kriminalität}{8}{subsection.5.1}
\contentsline {section}{\numberline {\oldstylenums {6}}Vergleich verschiedener Smart Contracts Plattformen}{8}{section.6}
\contentsline {subsection}{\numberline {\oldstylenums {\oldstylenums {6}.1}}Ethereum}{9}{subsection.6.1}
\contentsline {subsection}{\numberline {\oldstylenums {\oldstylenums {6}.2}}EOS}{9}{subsection.6.2}
\contentsline {subsection}{\numberline {\oldstylenums {\oldstylenums {6}.3}}Cardano}{10}{subsection.6.3}
\contentsline {subsection}{\numberline {\oldstylenums {\oldstylenums {6}.4}}IOTA}{11}{subsection.6.4}
\contentsline {subsection}{\numberline {\oldstylenums {\oldstylenums {6}.5}}Zusammenfassung}{11}{subsection.6.5}
\contentsline {section}{\numberline {\oldstylenums {7}}Ethische Aspekte}{13}{section.7}
\contentsline {section}{\numberline {\oldstylenums {8}}Fazit}{14}{section.8}
\contentsfinish 
