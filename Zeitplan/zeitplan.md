---
title: Zeitplan
header-includes: |
    \usepackage[margin=0.5in]{geometry}
    \usepackage[light]{CormorantGaramond}
    \usepackage{lipsum}
    \usepackage{fancyhdr}

numbersections: true
---

\maketitle

# Aufgabenstellung

*Smart contract engines are – next to cryptocurrencies like Bitcoin – probably the most important category of blockchain/DLT systems. Do some in-depth research and explain first what they are and what the potential of SC is. Which problems can we solve with them? Then describe the main characteristics of such platforms by comparing Ethereum, EOS, Cardano, and another one of your choice.*

# Ziele

- 2 Anwendungsbeispiele
- Unvoreingeommenes/neutrales Bild aus verschiedenen Blickwinkeln

# Struktur/Inhalt

Nur provisorisch!

- Begriffserklärung
- Nur im Bericht: Sporadische erklärung des blockchain-prinzips
- Funktionsprinzip
- Chancen/Potential
- Kritik/Gefahren
- Ethische Diskussion
- Offenes Ende $\rightarrow$ Zum Nachdenken Anregen

# Zeitplan

- 22. Oktober: Verständnis der Thematik, Foci sammeln
    - Nach Kurs: Zusammenkunft für Aufteilung und Aufbau, Zentrale Fragestellung
- 5. November: Informationen sammeln, stichpunktartig den Inhalt des jeweiligen Teils definieren
    - Nach Kurs: Besprechung, inputs, weitere Ideen
- 19. November: Schreibprozess, erste version. Gegenlesen, Kritik
    - nach kurs: Feedback, planung des vortrags
- 26. November: Slides, Gegenlesen
    - nach Kurs: Feedback, allfällige änderungen
- 3. Dezember: Jeder übt seinen Teil
- 9. Dezember: Hauptprobe vorüber, Einsenden des Handouts
- 10. Dezember: Vortrag


